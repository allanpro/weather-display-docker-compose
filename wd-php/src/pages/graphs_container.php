
<div class="box" id="homeContainer">
    <div class="" id="innercontentFixedSize">
        <h2><span style="display:inline-block; vertical-align:middle">Sidste 24 timer</span>&nbsp;&nbsp;<span class="blink" id="">Opdateres automatisk hvert 5. minut</span></h2>
        <div id="chartTempDew" class="charts">
            <div id="chartOverviewTempDew" class="overviewChart"></div>
        </div>
        <div id="chartHumidity" class="charts">
            <div id="chartOverviewHumidity" class="overviewChart"></div>
        </div>
        <div id="chartWind" class="charts">
            <div id="chartOverviewWind" class="overviewChart"></div>
        </div>
        <div id="chartWindDirection" class="charts">
            <div id="chartOverviewWindDirection" class="overviewChart"></div>
        </div>
        <div id="chartRainfall" class="charts">
            <div id="chartOverviewRainfall" class="overviewChart"></div>
        </div>
    </div>
</div>
