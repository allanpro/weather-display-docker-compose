<div class="box" id="homeContainer">
    <div class="" id="innercontentFixedSize">
        <h2><span style="display:inline-block; vertical-align:middle">Live</span>&nbsp;&nbsp;<span class="blink" id="liveTicker"></span></h2>
        <div id="liveTemp" class="overviewLive">
            <fieldset class="overviewFieldset">
                <legend><b>Temperatur</b></legend>
                <div id="bigLiveTemp" class="centeredHighlightedValue">
                    <span id="realTimeTemp"></span><br><span class="blink blinklivefont">LIVE</span>
                </div>
                <div id="detailLiveTemp" class="liveDetailValues">
                    <table class="tableSmallerFont">
                        <tr>
                            <th></th>
                            <th class="detailTh">Min</th>
                            <th class="detailTh">Max</th>
                        </tr>
                        <tr>

                            <td class="tableHeading">I dag</td>
                            <td class="detailTd"><span id="realtimeMinToday"></span>&nbsp;<span class="timeClass" id="realtimeMinTodayTime"></span></td>
                            <td class="detailTd"><span id="realtimeMaxToday"></span>&nbsp;<span class="timeClass" id="realtimeMaxTodayTime"></span></td>
                        </tr>
                        <tr>
                            <td class="tableHeading">I går</td>
                            <td class="detailTd"><span id="wdTempMinYesterday"></span>&nbsp;<span class="timeClass" id="realtimeMinYesterdayTime"></span></td>
                            <td class="detailTd"><span id="wdTempMaxYesterday"></span>&nbsp;<span class="timeClass" id="realtimeMaxYesterdayTime"></span></td>
                        </tr>
                        <tr>
                            <td class="tableHeading">Måned</td>
                            <td class="detailTd"><span id="wdTempMinMonth"></span></br><span class="timeClass" id="wdMinMonthTime"></span></td>
                            <td class="detailTd"><span id="wdTempMaxMonth"></span></br><span class="timeClass" id="wdMaxMonthTime"></span></td>
                        </tr>
                        <tr>
                            <td class="tableHeading">År</td>
                            <td class="detailTd"><span id="wdTempMinYear"></span></br><span class="timeClass" id="wdMinYearTime"></span></td></td>
                            <td class="detailTd"><span id="wdTempMaxYear"></span></br><span class="timeClass" id="wdMaxYearTime"></span></td></td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </div>

        <div id="liveRain" class="overviewLive">
            <fieldset class="overviewFieldset">
                <legend><b>Regn</b></legend>
                <div id="bigLiveRain" class="centeredHighlightedValue">
                    <span id="realTimeRain"></span><br><span style="font-size: 10px">IDAG (siden midnat)</span>
                </div>
                <div id="detailLiveRain" class="liveDetailValues">
                    <table class="tableSmallerFont">
                        <tr>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        <tr>
                            <td class="detailLeftTd">Nedbør igår</td>
                            <td class="detailRightTd"><span id="realtimeYesterdayRain"></span></td>
                        </tr>
                        <tr>
                            <td class="detailLeftTd">Nedbør siden mandag</td>
                            <td class="detailRightTd"><span id="wdRainCurrentWeek"></span></td>
                        </tr>
                        <tr>
                            <td class="detailLeftTd">Nedbør siden månedsstart</td>
                            <td class="detailRightTd"><span id="wdRainCurrentMonth"></span></td>
                        </tr>
                        <tr>
                            <td class="detailLeftTd">Regndage siden månedsstart</td>
                            <td class="detailRightTd"><span id="wdDaysWithRainThisMonth"></span></td>
                        </tr>
                        <tr>
                            <td class="detailLeftTd">Dage i træk uden regn</td>
                            <td class="detailRightTd"><span id="wdConsecutativeDaysWithNoRain"></span></td>
                        </tr>
                        <tr>
                            <td class="detailLeftTd">Sidste registreret regnvejr</td>
                            <td class="detailRightTd"><span id="wdLastRainStorm"></span></td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </div>

        <div id="liveWind" class="overviewLive">
            <fieldset class="overviewFieldset">
                <legend><b>Vind</b></legend>
                <div id="bigLiveWind" class="centeredHighlightedValue">
                    <span style="font-size: 10px">10-min gennemsnit</span><br><span id="realTimeWindSpeed"></span><br><span id="realTimeWindDir" style="font-size: 10px"></span>
                </div>
                <div id="detailLiveWind" class="liveDetailValues">
                    <table class="tableSmallerFont">
                        <tr>
                            <td width="55%" class="detailLeftTd">Kraftigste vindstød idag</td>
                            <td class="detailRightTd"><span id="wdMaxWindToday"></span></br><span class="timeClass" id="wdMaxWindTodayTime"></span></td>
                        </tr>
                        <tr>
                            <td class="detailLeftTd">Kraftigste vindstød i denne måned</td>
                            <td class="detailRightTd"><span id="wdMaxWindMonth"></span></br><span class="timeClass" id="wdMaxWindMonthTime"></span></td>
                        </tr>
                        <tr>
                            <td class="detailLeftTd">Kraftigste vindstød i år</td>
                            <td class="detailRightTd"><span id="wdMaxWindYear"></span></br><span class="timeClass" id="wdMaxWindYearTime"></span></td>
                        </tr>
                        <tr>
                            <td class="detailLeftTd">Kraftigste vindstød nogensinde</td>
                            <td class="detailRightTd"><span id="wdMaxWindEver"></span></br><span class="timeClass" id="wdMaxWindEverTime"></span></td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </div>

        <div id="liveOther" class="overviewLive">
            <fieldset class="overviewFieldset">
                <legend><b>Andet</b></legend>
                <div id="bigLiveOther" class="tableForOther">
                    <div class="liveOtherItem" style="float: left">
                        <span id="realTimeHumidity" style="font-size: 24px; font-weight: bolder"></span><br><span style="font-size: 10px">LUFTFUGTIGHED</span>
                    </div>
                    <div class="liveOtherItem" style="float: left">
                        <span id="realTimeBaro" style="font-size: 24px; font-weight: bolder"></span><br><span style="font-size: 10px">LUFTTRYK</span>
                    </div>
                    <div class="liveOtherItem" style="float: left">
                        <span id="realTimeDewPoint" style="font-size: 24px; font-weight: bolder"></span><br><span style="font-size: 10px">DUGPUNKT</span>
                    </div>
                    <div class="">
                        <div>
                            <div style="float: left; padding-left: 35px;"><span id="sunrise" style="font-size: 24px; font-weight: bolder"></span><br><span style="font-size: 10px">SOLOPGANG</span></div>
                            <div style="float: left;">&nbsp;&nbsp;&nbsp;</div>
                            <div style="float: left"><span id="sunset" style="font-size: 24px; font-weight: bolder"></span><br><span style="font-size: 10px">SOLNEDGANG</span></div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>