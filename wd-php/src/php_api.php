<?php

include 'wd/testtags.php';
$result = array();
//file_put_contents('php://stderr', print_r('maxtempyester: ' . $maxtempyest, TRUE));
$result["maxtempyest"] = html_entity_decode(utf8_encode($maxtempyest));
$result["mintempyest"] = html_entity_decode(utf8_encode($mintempyest));
$result["mrecordhightemp"] = utf8_encode($mrecordhightemp);
$result["mrecordlowtemp"] = utf8_encode($mrecordlowtemp);
$result["yrecordhightemp"] = utf8_encode($yrecordhightemp);
$result["yrecordlowtemp"] = utf8_encode($yrecordlowtemp);
$result["mintempt"] = utf8_encode($mintempt);
$result["maxtempt"] = utf8_encode($maxtempt);
$result["mintempyestt"] = utf8_encode($mintempyestt);
$result["maxtempyestt"] = utf8_encode($maxtempyestt);
$result["minbaroyest"] = utf8_encode($minbaroyest);
$result["maxbaroyest"] = utf8_encode($maxbaroyest);
$result["minbaroyestt"] = utf8_encode($minbaroyestt);
$result["maxbaroyestt"] = utf8_encode($maxbaroyestt);

$result["mrecordlowtempday"] = utf8_encode($mrecordlowtempday);
$result["mrecordlowtempmonth"] = utf8_encode($mrecordlowtempmonth);
$result["mrecordlowtempyear"] = utf8_encode($mrecordlowtempyear);

$result["mrecordhightempday"] = utf8_encode($mrecordhightempday);
$result["mrecordhightempmonth"] = utf8_encode($mrecordhightempmonth);
$result["mrecordhightempyear"] = utf8_encode($mrecordhightempyear);

$result["yrecordhightempday"] = utf8_encode($yrecordhightempday);
$result["yrecordhightempmonth"] = utf8_encode($yrecordhightempmonth);
$result["yrecordhightempyear"] = utf8_encode($yrecordhightempyear);

$result["yrecordlowtempday"] = utf8_encode($yrecordlowtempday);
$result["yrecordlowtempmonth"] = utf8_encode($yrecordlowtempmonth);
$result["yrecordlowtempyear"] = utf8_encode($yrecordlowtempyear);

$result["raincurrentweek"] = utf8_encode($raincurrentweek);
$result["monthrn"] = utf8_encode($monthrn);
$result["dayswithrain"] = utf8_encode($dayswithrain);
$result["dayswithnorain"] = utf8_encode($dayswithnorain);
$result["dateoflastrainalways"] = utf8_encode($dateoflastrainalways);
$result["timeoflastrainalways"] = utf8_encode($timeoflastrainalways);

$result["mrecordlowbaro"] = utf8_encode($mrecordlowbaro);
$result["mrecordlowbaroday"] = utf8_encode($mrecordlowbaroday);
$result["mrecordlowbaromonth"] = utf8_encode($mrecordlowbaromonth);
$result["mrecordlowbaroyear"] = utf8_encode($mrecordlowbaroyear);

$result["mrecordhighbaro"] = utf8_encode($mrecordhighbaro);
$result["mrecordhighbaroday"] = utf8_encode($mrecordhighbaroday);
$result["mrecordhighbaromonth"] = utf8_encode($mrecordhighbaromonth);
$result["mrecordhighbaroyear"] = utf8_encode($mrecordhighbaroyear);

$result["yrecordlowbaro"] = utf8_encode($yrecordlowbaro);
$result["yrecordlowbaroday"] = utf8_encode($yrecordlowbaroday);
$result["yrecordlowbaromonth"] = utf8_encode($yrecordlowbaromonth);
$result["yrecordlowbaroyear"] = utf8_encode($yrecordlowbaroyear);

$result["yrecordhighbaro"] = utf8_encode($yrecordhighbaro);
$result["yrecordhighbaroday"] = utf8_encode($yrecordhighbaroday);
$result["yrecordhighbaromonth"] = utf8_encode($yrecordhighbaromonth);
$result["yrecordhighbaroyear"] = utf8_encode($yrecordhighbaroyear);


//
//
//
//$yrecordwindgust = '28.8';
//$yrecordhighgustday = '28';
//$yrecordhighgustmonth = '10';
//$yrecordhighgustyear = '2013';
//
//
//$recordhighgustmonth = '10';
//$recordhighgustday = '28';
//$recordhighgustyear = '2013';
//
//$mrecordwindgust =  '15.7';	// All time record high wind gust
//$mrecordhighgustday =  '3';	// Day of record high wind gust
//
//$maxgustyest = '13.0 m/s SSW';
//$maxgustyestt = '03:25';
//
//


echo json_encode($result);



