<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">

<?php
    require_once('headHD.php');
?>

    <div class="main" id="container">

        <div class="box" id="header">
            <h1>juelsminde-vejret.dk</h1>
        </div>

        <?php
        require_once('pages/livedata_container.php');
        ?>

    </div>
<script>
    updateLiveDashboard();
    updateOverviewGraphs(document.getElementById('chartOverviewTempDew'), document.getElementById('chartOverviewHumidity'), document.getElementById('chartOverviewWind'), document.getElementById('chartOverviewWindDirection'), document.getElementById('chartOverviewRainfall'), 24 );
    window.setInterval(
        function() {one_second_passed_update_ui();},
        1000);
    setBackgroundBasedOnSeason();
</script>

<!-- HighCharts Dependency -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/4.2.6/highcharts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/4.2.6/themes/gray.js"></script>

</body>
</html>