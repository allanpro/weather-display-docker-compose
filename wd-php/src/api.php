<?php

//--------------------------------------------------------------------------
// 1) Connect to mysql database
//--------------------------------------------------------------------------
include 'DB.php';
$link = mysqli_connect($host, $user, $pass);
$dbs = mysqli_select_db($link, $databaseName);

//--------------------------------------------------------------------------
// 2) Query database for data
//--------------------------------------------------------------------------
$dateformat = 'Y-m-d H:i:s';
$queryType = $_REQUEST['queryType'];
if ("tempdewhumid" == $queryType) {
    $dateTimeFetchDataFrom = new DateTime();
    $dateTimeFetchDataFrom->sub(new DateInterval('PT' . intval($_REQUEST['hoursOfData']) .  'H'));

    $query = "SELECT time, date, ROUND(AVG(temperature), 1) as temperature, ROUND(AVG(dew_point_temperature), 1) as dew_point_temperature, ROUND(AVG(outdoor_humidity), 0) as outdoor_humidity, MAX(ROUND( average_windspeed / 3.6, 1 )) AS average_windspeed, MAX(ROUND( gust_windspeed / 3.6, 1 )) AS gust_windspeed, AVG(wind_direction) as wind_direction, ROUND(AVG(barometer), 0) as barometer, MAX(ROUND(daily_rainfall,1)) as daily_rainfall FROM $tableName WHERE datetime >= '" . $dateTimeFetchDataFrom->format('YmdHis') . "' GROUP BY UNIX_TIMESTAMP(datetime) DIV 600 ORDER BY DATETIME DESC";

    $queryResult = mysqli_query($link, $query);
    $dateTimeArray = array();
    $tempArray = array();
    $dewPointArray = array();
    $humidityArray = array();
    $windAvgArray = array();
    $windGustArray = array();
    $windDirectionArray = array();
    $barometerArray = array();
    $dailyRainfallArray = array();

    while ($row = mysqli_fetch_assoc($queryResult)) {
        $dateTimeArray[] = DateTime::createFromFormat($dateformat, $row['date'] . $row['time']);
        $tempArray[] = floatval($row['temperature']);
        $dewPointArray[] = floatval($row['dew_point_temperature']);
        $humidityArray[] = floatval($row['outdoor_humidity']);
        $windAvgArray[] = floatval($row['average_windspeed']);
        $windGustArray[] = floatval($row['gust_windspeed']);
        $windDirectionArray[] = intval($row['wind_direction']);
        $barometerArray[] = floatval($row['barometer']);
        $dailyRainfallArray[] = floatval($row['daily_rainfall']);
    }

    $result = array();
    $result[] = array_reverse($dateTimeArray);
    $result[] = array_reverse($tempArray);
    $result[] = array_reverse($dewPointArray);
    $result[] = array_reverse($humidityArray);
    $result[] = array_reverse($windAvgArray);
    $result[] = array_reverse($windGustArray);
    $result[] = array_reverse($windDirectionArray);
    $result[] = array_reverse($barometerArray);
    $result[] = array_reverse($dailyRainfallArray);

    echo json_encode($result);
}

if ("temptoday" == $queryType) {
    $dateTimeFetchDataFrom = new DateTime();
    $dateTimeFetchDataFrom->sub(new DateInterval('PT24H'));
    $sql = "SELECT time, date, ROUND(AVG(temperature), 1) as temperature, ROUND(AVG(min_daily_temperature), 1) as min_daily_temperature, ROUND(AVG(max_daily_temperature), 1) as max_daily_temperature FROM wd_history WHERE datetime >= '" . $dateTimeFetchDataFrom->format('YmdHis') . "' GROUP BY UNIX_TIMESTAMP(datetime) DIV 300 ORDER BY DATETIME DESC";
    $queryResult = mysqli_query($link, $sql);
    $dateTimeTempArray = array();
    $dateTimeTempRangeArray = array();

    while ($row = mysqli_fetch_assoc($queryResult)) {
        $dateTime = DateTime::createFromFormat($dateformat, $row['date'] . $row['time']);
        $dateTimeTempArray[] = array($dateTime, floatval($row['temperature']));
        $dateTimeTempRangeArray[] = array($dateTime, floatval($row['min_daily_temperature']), floatval($row['max_daily_temperature']));
    }

    $result = array();
    $result[] = array_reverse($dateTimeTempArray);
    $result[] = array_reverse($dateTimeTempRangeArray);
    echo json_encode($result);
}

if ("baromintimetoday" == $queryType) {
    $query = "SELECT barometer, datetime, date, time FROM wd_history wd1 WHERE date >= CURDATE() order by barometer desc, time desc LIMIT 1";
    $queryResult = mysqli_query($link, $query);
    $row = mysqli_fetch_assoc($queryResult);
    $result = DateTime::createFromFormat($dateformat, $row['date'] . $row['time']);
    echo json_encode($result);
}

if ("baromaxtimetoday" == $queryType) {
    $query = "SELECT barometer, datetime, date, time FROM wd_history wd1 WHERE date >= CURDATE() order by barometer desc, time asc LIMIT 1";
    $queryResult = mysqli_query($link, $query);
    $row = mysqli_fetch_assoc($queryResult);
    $result = DateTime::createFromFormat($dateformat, $row['date'] . $row['time']);
    echo json_encode($result);
}

?>

