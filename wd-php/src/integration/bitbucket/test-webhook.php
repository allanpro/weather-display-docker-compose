<?php
/**
 * Created by PhpStorm.
 * User: alaurids
 * Date: 09-12-13
 * Time: 11:42
 */

$json_url = 'http://juelsminde-vejret.dk/integration/bitbucket/webhook.php';

$payload = 'payload=%7B%22repository%22%3A+%7B%22website%22%3A+%22%22%2C+%22fork%22%3A+false%2C+%22name%22%3A+%22juelsminde-vejret.dk%22%2C+%22scm%22%3A+%22git%22%2C+%22owner%22%3A+%22allanpro%22%2C+%22absolute_url%22%3A+%22%2Fallanpro%2Fjuelsminde-vejret.dk%2F%22%2C+%22slug%22%3A+%22juelsminde-vejret.dk%22%2C+%22is_private%22%3A+true%7D%2C+%22truncated%22%3A+false%2C+%22commits%22%3A+%5B%7B%22node%22%3A+%22ff1a8ecb3462%22%2C+%22files%22%3A+%5B%7B%22type%22%3A+%22modified%22%2C+%22file%22%3A+%22index.php%22%7D%5D%2C+%22branch%22%3A+%22master%22%2C+%22utctimestamp%22%3A+%222013-12-09+09%3A59%3A59%2B00%3A00%22%2C+%22timestamp%22%3A+%222013-12-09+10%3A59%3A59%22%2C+%22raw_node%22%3A+%22ff1a8ecb346267bda64d30ffab60267d6f0a8e57%22%2C+%22message%22%3A+%22Changed+texts+with+rain.%5Cn%22%2C+%22size%22%3A+-1%2C+%22author%22%3A+%22allanpro%22%2C+%22parents%22%3A+%5B%22f6c4dc141c38%22%5D%2C+%22raw_author%22%3A+%22Allan+Lauridsen+%3Callan.lauridsen%40gmail.com%3E%22%2C+%22revision%22%3A+null%7D%5D%2C+%22canon_url%22%3A+%22https%3A%2F%2Fbitbucket.org%22%2C+%22user%22%3A+%22allanpro%22%7D';

// Initializing curl
$ch = curl_init( $json_url );

// Configuring curl options
$options = array(
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTPHEADER => array('Content-type: application/x-www-form-urlencoded') ,
    CURLOPT_POSTFIELDS => $payload
);

// Setting curl options
curl_setopt_array( $ch, $options );

// Getting results
$result =  curl_exec($ch); // Getting jSON result string