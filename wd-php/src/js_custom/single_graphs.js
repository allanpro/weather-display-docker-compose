function drawTempWithMaxMinRange(element) {
    $.ajax({
        url: 'api.php',
        data: {"queryType":'temptoday'},         //for example "id=5&parent=6"
        dataType: 'json',
        success: function (data)
        {
            var dateTimeTempArray = new Array();
            var dateTimeTempRangeArray = new Array();
            var dateTimeTempData=data[0];
            var dateTimeTempRangeData=data[1];
            var len=dateTimeTempData.length;
            for(var i=0; i<len; i++) {
                dateTimeTempArray.push([moment(dateTimeTempData[i][0].date, "YYYY-MM-DD HH:mm:ss").valueOf(), dateTimeTempData[i][1]]);
                dateTimeTempRangeArray.push([moment(dateTimeTempRangeData[i][0].date, "YYYY-MM-DD HH:mm:ss").valueOf(), dateTimeTempRangeData[i][1], dateTimeTempRangeData[i][2]]);
            }

            $(element).highcharts({
                chart: {
                    renderTo: element
                },
                title: {
                    text: 'July temperatures'
                },

                xAxis: {
                    type: 'datetime'
                },

                yAxis: {
                    title: {
                        text: null
                    }
                },

                tooltip: {
                    crosshairs: true,
                    shared: true,
                    valueSuffix: '°C'
                },

                legend: {
                },

                series: [{
                    name: 'Temperature',
                    data: dateTimeTempArray,
                    zIndex: 1,
                    marker: {
                        fillColor: 'white',
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[0]
                    }
                }, {
                    name: 'Range',
                    data: dateTimeTempRangeArray,
                    type: 'arearange',
                    lineWidth: 0,
                    linkedTo: ':previous',
                    color: Highcharts.getOptions().colors[0],
                    fillOpacity: 0.3,
                    zIndex: 0
                }]

            });

        }
    });
}
