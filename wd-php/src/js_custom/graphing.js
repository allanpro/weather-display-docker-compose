function updateOverviewGraphs(elementTempDew, elementHumidity, elementWindSpeed, elementWindDirection, elementRainfall, hoursOfData) {
    $.ajax({
        url: 'api.php',
        data: {"hoursOfData":hoursOfData,
            "queryType":'tempdewhumid'},         //for example "id=5&parent=6"
        dataType: 'json',
        success: function (data)
        {
            drawTempDewOverviewGraph(elementTempDew, data);
            drawHumidityOverviewGraph(elementHumidity, data);
            drawWindSpeedOverviewGraph(elementWindSpeed, data);
            drawWindDirectionOverviewGraph(elementWindDirection, data);
            drawRainfallOverviewGraph(elementRainfall, data);
        }
    });
}

function drawTempDewOverviewGraph(element, data) {
    var timeDataTemperatureArray = new Array();
    var timeDataDewPointArray = new Array();
    var timeArray=data[0];
    var temperatureArray=data[1];
    var dewPointArray=data[2];
    var len=timeArray.length;
    for(var i=0; i<len; i++) {
        timeDataTemperatureArray.push([moment(timeArray[i].date, "YYYY-MM-DD HH:mm:ss").valueOf(), temperatureArray[i]]);
        timeDataDewPointArray.push([moment(timeArray[i].date, "YYYY-MM-DD HH:mm:ss").valueOf(), dewPointArray[i]]);
    }

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var timerText = "";

    $(element).highcharts({
        chart: {
            zoomType: '',
            backgroundColor: 'rgba(83, 83, 83, .6)'
        },
        title: {
            text: 'Temperatur og dugpunkt'
        },
        subtitle: {
            text: timerText
        },
        xAxis: [
            {
                type: 'datetime',
                gridLineWidth: 0,
                dateTimeLabelFormats: {
                    day: '%H:%M'
                },
                startOnTick: 'false',
                endOnTick: 'false',
                tickInterval: 3600 * 1000 * 4
            }
        ],
        yAxis: [
            { // Temperature yAxis
                labels: {
                    formatter: function () {
                        return this.value + ' °C';
                    },
                    style: {
                        color: '#FA5858'
                    }
                },
                title: {
                    text: ''
                },
                opposite: false,
                minPadding: '0.01',
                maxPadding: '0.01',
                tickInterval: 1
            }
        ],
        tooltip: {
            shared: true,
            crosshairs: true,
            followTouchMove: false
        },
        legend: {
          enabled: false
        },
        series: [
            {
                name: 'Temperatur',
                color: '#FA5858',
                type: 'spline',
                data: timeDataTemperatureArray,
                tooltip: {
                    valueSuffix: ' °C'
                },
                marker: {
                    enabled: false
                },
                dashStyle: 'solid',
                lineWidth: 2,
                enableMouseTracking: true

            },             {
                name: 'Dugpunkt',
                color: '#A4A4A4',
                type: 'spline',
                data: timeDataDewPointArray,
                tooltip: {
                    valueSuffix: ' °C'
                },
                marker: {
                    enabled: false
                },
                dashStyle: 'solid',
                lineWidth: 2,
                enableMouseTracking: true

            }
        ]
    });
}

function drawHumidityOverviewGraph(element, data) {
    var timeDataHumidityArray = new Array();
    var timeDataBarometerArray = new Array();
    var timeArray=data[0];
    var humidityArray=data[3];
    var barometerArray=data[7];
    var len=timeArray.length;
    for(var i=0; i<len; i++) {
        timeDataHumidityArray.push([moment(timeArray[i].date, "YYYY-MM-DD HH:mm:ss").valueOf(), humidityArray[i]]);
        timeDataBarometerArray.push([moment(timeArray[i].date, "YYYY-MM-DD HH:mm:ss").valueOf(), barometerArray[i]]);
    }

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var timerText = "";

    $(element).highcharts({
        chart: {
            zoomType: '',
            backgroundColor: 'rgba(83, 83, 83, .6)'
        },
        title: {
            text: 'Luftfugtighed og lufttryk'
        },
        subtitle: {
            text: timerText
        },
        xAxis: [
            {
                type: 'datetime',
                dateTimeLabelFormats: {
                    day: '%H:%M'
                },
                startOnTick: 'false',
                endOnTick: 'false',
                tickInterval: 3600 * 1000 * 4
            }
        ],
        yAxis: [
            {
                labels: {
                    formatter: function () {
                        return this.value + '%';
                    },
                    style: {
                        color: '#5858FA'
                    }
                },
                title: {
                    text: ''
                },
                opposite: false,
                minPadding: '0.01',
                maxPadding: '0.01',
                min: 30,
                max: 100,
                tickInterval: 10
            },
            {
                labels: {
                    formatter: function () {
                        return this.value + 'hPa';
                    },
                    style: {
                        color: '#FA58AC'
                    }
                },
                title: {
                    text: ''
                },
                opposite: true,
                minPadding: '0.01',
                maxPadding: '0.01',
                min: 970,
                max: 1040,
                tickInterval: 10
            }
        ],
        tooltip: {
            shared: true,
            crosshairs: true,
            followTouchMove: false
        },
        legend: {
            enabled: false
        },
        series: [
            {
                name: 'Luftfugtighed',
                color: '#5858FA',
                type: 'spline',
                data: timeDataHumidityArray,
                tooltip: {
                    valueSuffix: '%'
                },
                marker: {
                    enabled: false
                },
                dashStyle: 'solid',
                lineWidth: 2,
                enableMouseTracking: true
            },
            {
                name: 'Lufttryk',
                color: '#FA58AC',
                type: 'spline',
                data: timeDataBarometerArray,
                tooltip: {
                    valueSuffix: ' hPa'
                },
                marker: {
                    enabled: false
                },
                dashStyle: 'solid',
                lineWidth: 2,
                yAxis: 1,
                enableMouseTracking: true
            }
        ]
    });
}

function drawWindSpeedOverviewGraph(element, data) {
    var timeDataWindAvgArray = new Array();
    var timeDataWindGustArray = new Array();
    var timeArray = data[0];
    var windAvgArray = data[4];
    var windGustArray = data[5];
    var len = timeArray.length;
    for (var i = 0; i < len; i++) {
        timeDataWindAvgArray.push([moment(timeArray[i].date, "YYYY-MM-DD HH:mm:ss").valueOf(), windAvgArray[i]]);
        timeDataWindGustArray.push([moment(timeArray[i].date, "YYYY-MM-DD HH:mm:ss").valueOf(), windGustArray[i]]);
    }

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var timerText = "";

    $(element).highcharts({
        chart: {
            zoomType: '',
            backgroundColor: 'rgba(83, 83, 83, .6)'
        },
        title: {
            text: 'Middelvind og vindstød'
        },
        subtitle: {
            text: timerText
        },
        xAxis: [
            {
                type: 'datetime',
                dateTimeLabelFormats: {
                    day: '%H:%M'
                },
                startOnTick: 'false',
                endOnTick: 'false',
                tickInterval: 3600 * 1000 * 4
            }
        ],
        yAxis: [
            { // Humidity yAxis
                labels: {
                    formatter: function () {
                        return this.value + ' m/s';
                    },
                    style: {
                        color: '#64FE2E'
                    }
                },
                title: {
                    text: ''
                },
                tickInterval: 1,
                opposite: false,
                minPadding: '0.01',
                maxPadding: '0.01',
                min: 0,
                plotBands: [{ // Light air
                    from: 0.3,
                    to: 1.5,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'Næsten stille',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Light breeze
                    from: 1.5,
                    to: 3.5,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'Svag vind',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Gentle breeze
                    from: 3.5,
                    to: 5.5,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'Let vind',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Moderate breeze
                    from: 5.5,
                    to: 7.5,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'Jævn vind',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Fresh breeze
                    from: 7.5,
                    to: 10.5,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'Frisk vind',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Strong breeze
                    from: 10.5,
                    to: 13.5,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'Hård vind',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // High wind
                    from: 13.5,
                    to: 16.5,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'Stiv kuling',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // High wind
                    from: 16.5,
                    to: 20.5,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'Hård kuling',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // High wind
                    from: 20.5,
                    to: 24.5,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'Stormende kuling',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // High wind
                    from: 24.5,
                    to: 28.5,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'Storm',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // High wind
                    from: 28.5,
                    to: 32.5,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'Stærk storm',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // High wind
                    from: 32.5,
                    to: 100,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'Orkan',
                        style: {
                            color: '#606060'
                        }
                    }

                }]
            }
        ],
        tooltip: {
            shared: true,
            crosshairs: true,
            followTouchMove: false
        },
        legend: {
            enabled: false
        },
        series: [
            {
                name: 'Vindstød',
                color: '#FE2E64',
                type: 'spline',
                data: timeDataWindGustArray,
                tooltip: {
                    valueSuffix: ' m/s'
                },
                marker: {
                    enabled: false
                },
                dashStyle: 'solid',
                lineWidth: 2,
                enableMouseTracking: true
            },
            {
                name: 'Middelvind - løbende gennemsnit (10 min)',
                color: '#64FE2E',
                type: 'spline',
                data: timeDataWindAvgArray,
                tooltip: {
                    valueSuffix: ' m/s'
                },
                marker: {
                    enabled: false
                },
                dashStyle: 'solid',
                lineWidth: 2,
                enableMouseTracking: true

            }
        ]
    });
}

function drawWindDirectionOverviewGraph(element, data) {
    var timeDataWindDirectionArray = new Array();
    var timeArray = data[0];
    var windDirectionArray = data[6];
    var len = timeArray.length;
    for (var i = 0; i < len; i++) {
        timeDataWindDirectionArray.push([moment(timeArray[i].date, "YYYY-MM-DD HH:mm:ss").valueOf(), windDirectionArray[i]]);
    }

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var timerText = "";

    $(element).highcharts({
        chart: {
            type: 'scatter',
            zoomType: '',
            backgroundColor: 'rgba(83, 83, 83, .6)'
        },
        title: {
            text: 'Vindretning fra'
        },
        subtitle: {
            text: timerText
        },
        tooltip: {
            shared: true,
            crosshairs: true,
            followTouchMove: false
        },
        xAxis: [
            {
                type: 'datetime',
                dateTimeLabelFormats: {
                    day: '%H:%M'
                },
                startOnTick: 'false',
                endOnTick: 'false',
                tickInterval: 3600 * 1000 * 4
            }
        ],
        yAxis: {
            tooltip: {
                shared: true,
                crosshairs: true
            },
            min: 0,
            max: 360,
            tickInterval: 90,
            title: {
                text: ''
            },
            labels: {
                formatter: function () {
                    return this.value + '°';
                },
                style: {
                    color: '#ACFA58'
                }
            },
            plotBands: [{
                from: 0,
                to: 45,
                color: 'rgba(68, 170, 213, 0.1)',
                label: {
                    text: 'Nord',
                    style: {
                        color: '#606060'
                    }
                }
            }, {
                from: 45,
                to: 135,
                color: 'rgba(0, 0, 0, 0)',
                label: {
                    text: 'Øst',
                    style: {
                        color: '#606060'
                    }
                }
            }, {
                from: 135,
                to: 225,
                color: 'rgba(68, 170, 213, 0.1)',
                label: {
                    text: 'Syd',
                    style: {
                        color: '#606060'
                    }
                }
            }, {
                from: 225,
                to: 315,
                color: 'rgba(0, 0, 0, 0)',
                label: {
                    text: 'Vest',
                    style: {
                        color: '#606060'
                    }
                }
            }, {
                from: 315,
                to: 360,
                color: 'rgba(68, 170, 213, 0.1)',
                label: {
                    text: 'Nord',
                    style: {
                        color: '#606060'
                    }
                }
            }]
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 1,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.y}°'
                }
            }
        },
        series: [
            {
                name: 'Vindretning',
                color: '#ACFA58',
                data: timeDataWindDirectionArray,
                enableMouseTracking: true
           }
        ]
    });
}

function setMinBaroTimeToday(element) {
    $.ajax({
        url: 'api.php',
        data: {"queryType":'baromintimetoday'},
        dataType: 'json',
        success: function (data)
        {

            $(element).text("(kl. " + moment(data["date"]).format('HH:mm') + ")");
        }
    });
}

function setMaxBaroTimeToday(element) {
    $.ajax({
        url: 'api.php',
        data: {"queryType":'baromaxtimetoday'},
        dataType: 'json',
        success: function (data)
        {

            $(element).text("(kl. " + moment(data["date"]).format('HH:mm') + ")");
        }
    });
}

function drawRainfallOverviewGraph(element, data) {
    var timeDataRainfallArray = new Array();
    var timeArray=data[0];
    var rainfallArray=data[8];
    var len=timeArray.length;
    for(var i=0; i<len; i++) {
        timeDataRainfallArray.push([moment(timeArray[i].date, "YYYY-MM-DD HH:mm:ss").valueOf(), rainfallArray[i]]);
    }

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var timerText = "";

    $(element).highcharts({
        chart: {
            zoomType: '',
            backgroundColor: 'rgba(83, 83, 83, .6)'
        },
        title: {
            text: 'Nedbør'
        },
        subtitle: {
            text: timerText
        },
        xAxis: [
            {
                type: 'datetime',
                gridLineWidth: 0,
                dateTimeLabelFormats: {
                    day: '%H:%M'
                },
                startOnTick: 'false',
                endOnTick: 'false',
                tickInterval: 3600 * 1000 * 4
            }
        ],
        yAxis: [
            {
                labels: {
                    formatter: function () {
                        return this.value + ' mm';
                    },
                    style: {
                        color: '#5858FA'
                    }
                },
                title: {
                    text: ''
                },
                opposite: false,
                minPadding: '0.01',
                maxPadding: '0.01',

            }
        ],
        tooltip: {
            shared: true,
            crosshairs: true,
            followTouchMove: false
        },
        legend: {
            enabled: false
        },
        series: [
            {
                name: 'Nedbør',
                color: '#5858FA',
                type: 'spline',
                data: timeDataRainfallArray,
                tooltip: {
                    valueSuffix: ' mm'
                },
                marker: {
                    enabled: false
                },
                dashStyle: 'solid',
                lineWidth: 2,
                enableMouseTracking: true

            }
        ]
    });
}