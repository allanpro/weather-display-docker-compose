var counterSecs = 0;

function one_second_passed_update_ui() {
    if (counterSecs != 0 && ((counterSecs % 15) == 0)) {
        // update live dashboard numbers every 15 seconds and at the beginning
        updateLiveDashboard();
    }
    if (counterSecs != 0 && ((counterSecs % 300) == 0)) {
        // update live graphs every 5 minutes and at the beginning
        updateOverviewGraphs(document.getElementById('chartOverviewTempDew'), document.getElementById('chartOverviewHumidity'), document.getElementById('chartOverviewWind'), document.getElementById('chartOverviewWindDirection'), document.getElementById('chartOverviewRainfall'), 24 );
    }
    lastUpdateTime.lang("da");
    $("#liveTicker").text("Opdateres automatisk " + " (" + lastUpdateTime.format('Do MMMM YYYY, HH:mm:ss') + ")");
    counterSecs++;
}

function setBackgroundBasedOnSeason() {
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month >= 3 && month <6) { // spring
        $('html').css('background-image', 'url("../img/spring.jpg")');
    } else if (month >= 6 && month <9) { // summer
        $('html').css('background-image', 'url("../img/summer.jpg")');
    } else if (month >= 9 && month <12) { //autumn
        $('html').css('background-image', 'url("../img/autumn2.jpg")');
    } else { // winter
        $('html').css('background-image', 'url("../img/winter.jpg")');
    }

}
