var lastClientRaw = null;
var lastClientRawExtra = null;
var lastTestTags = null;
var lastUpdateTime = null;

function updateLiveDashboard() {
    // retrieve raw data from WD
    $.get( "wd/clientraw.txt", function( data ) {
        if (lastClientRaw == null || ((lastClientRaw.toString().localeCompare(data.toString())) != 0)) {
            lastClientRaw = data;
            lastUpdateTime = moment();
            var clientraw = data.split(' ');
            // now make sure we got the entire clientraw.txt
            // valid clientraw.txt has '12345' at start and '!!' at end of record
            var wdpattern=/\d+\.\d+.*!!/; // looks for '!!nn.nn!!' version string
            // If we have a valid clientraw file AND updates is < maxupdates
            if(clientraw[0] != '12345' || !wdpattern.test(data)) {
                // clientraw.txt format was no good!
                return;
            }
            // start updating the UI
            $("#realTimeTemp").text(clientraw[4] + " °C");
            $("#realTimeRain").text(clientraw[7] + " mm");
            $("#realTimeWindSpeed").text(convertWindSpeed(clientraw[158]) + " m/s");
            $("#realTimeWindDir").text(convertWindDir(clientraw[3]));
            $("#realTimeHumidity").text(clientraw[5] + "%");
            $("#realTimeBaro").text(clientraw[6] + " hPa");
            $("#realTimeDewPoint").text(clientraw[72] + " °C");
            $("#realtimeMinToday").text(clientraw[47] + " °C");
            $("#realtimeMaxToday").text(clientraw[46] + " °C");
            $("#realtimeYesterdayRain").text(clientraw[19] + " mm");
            $("#wdMaxWindToday").text(convertWindSpeed(clientraw[71]) + " m/s");
            $("#wdMaxWindTodayTime").text("(kl. " + clientraw[135] + ")");
            $("#realtimeMinBaroToday").text(clientraw[132] + " hPa");
            $("#realtimeMaxBaroToday").text(clientraw[131] + " hPa");
        }
    });

    setMinBaroTimeToday($("#realtimeMinBaroTodayTime"));
    setMaxBaroTimeToday($("#realtimeMaxBaroTodayTime"));

    $.get( "wd/clientrawextra.txt", function( data ) {
        if (lastClientRawExtra == null || (lastClientRawExtra.toString().localeCompare(data.toString()) != 0)) {
            lastClientRawExtra = data;
            lastUpdateTime = moment();
            var clientraw = data.split(' ');
            // now make sure we got the entire clientraw.txt
            // valid clientraw.txt has '12345' at start and '!!' at end of record
            var wdpattern=/\d+\.\d+.*!!/; // looks for '!!nn.nn!!' version string
            // If we have a valid clientraw file AND updates is < maxupdates
            if(clientraw[0] != '12345' || !wdpattern.test(data)) {
                // clientraw.txt format was no good!
                return;
            }
            // start updating the UI
            $("#sunrise").text(clientraw[556]);
            $("#sunset").text(clientraw[557]);
            $("#wdMaxWindMonth").text(convertWindSpeed(clientraw[73]) + " m/s");
            $("#wdMaxWindMonthTime").text("(d. " + clientraw[76] + "/" + clientraw[77] + "-" + clientraw[78] + " kl. " + clientraw[74] + "." + clientraw[75] + ")");
            $("#wdMaxWindYear").text(convertWindSpeed(clientraw[199]) + " m/s");
            $("#wdMaxWindYearTime").text("(d. " + clientraw[202] + "/" + clientraw[203] + "-" + clientraw[204] + " kl. " + clientraw[200] + "." + clientraw[201] + ")");
            $("#wdMaxWindEver").text(convertWindSpeed(clientraw[325]) + " m/s");
            $("#wdMaxWindEverTime").text("(d. " + clientraw[328] + "/" + clientraw[329] + "-" + clientraw[330] + " kl. " + clientraw[326] + "." + clientraw[327] + ")");
        }
    });

    $.ajax({
        url: 'php_api.php',
        dataType: 'json',
        success: function (data)
        {

            console.log('jsonEqual(lastTestTags, data): ' + jsonEqual(lastTestTags, data));
            //console.log('Object.deepEquals(lastTestTags, data): ' + Object.deepEquals(lastTestTags, data));
            if (lastTestTags == null || !(jsonEqual(lastTestTags, data)) ) {
                lastTestTags = data;
                lastUpdateTime = moment();
                console.log('updating latestTestTags data...');
                $("#realtimeMinTodayTime").text("(kl. " + data["mintempt"] + ")");
                $("#realtimeMaxTodayTime").text("(kl. " + data["maxtempt"] + ")");
                $("#wdTempMinYesterday").text(data["mintempyest"]);
                $("#wdTempMaxYesterday").text(data["maxtempyest"]);
                $("#realtimeMinYesterdayTime").text("(kl. " + data["mintempyestt"] + ")");
                $("#realtimeMaxYesterdayTime").text("(kl. " + data["maxtempyestt"] + ")");
                $("#wdTempMinMonth").text(data["mrecordlowtemp"] + " °C");
                $("#wdTempMaxMonth").text(data["mrecordhightemp"] + " °C");
                $("#wdTempMinYear").text(data["yrecordlowtemp"] + " °C");
                $("#wdTempMaxYear").text(data["yrecordhightemp"] + " °C");
                $("#wdMinMonthTime").text("(d. " + data["mrecordlowtempday"] + "/" + data["mrecordlowtempmonth"] + "-" + data["mrecordlowtempyear"] + ")");
                $("#wdMaxMonthTime").text("(d. " + data["mrecordhightempday"] + "/" + data["mrecordhightempmonth"] + "-" + data["mrecordhightempyear"] + ")");
                $("#wdMinYearTime").text("(d. " + data["yrecordlowtempday"] + "/" + data["yrecordlowtempmonth"] + "-" + data["yrecordlowtempyear"] + ")");
                $("#wdMaxYearTime").text("(d. " + data["yrecordhightempday"] + "/" + data["yrecordhightempmonth"] + "-" + data["yrecordhightempyear"] + ")");
                $("#wdRainCurrentWeek").text(data["raincurrentweek"] + " mm");
                $("#wdRainCurrentMonth").text(data["monthrn"]);
                $("#wdDaysWithRainThisMonth").text(data["dayswithrain"] + " " + dageEllerDag(data["dayswithrain"]));
                $("#wdConsecutativeDaysWithNoRain").text(data["dayswithnorain"] + " " + dageEllerDag(data["dayswithnorain"]));
                $("#wdLastRainStorm").text(data["dateoflastrainalways"] + " kl." + data["timeoflastrainalways"]);
                $("#wdBaroMinYesterday").text(data["minbaroyest"]);
                $("#wdBaroMaxYesterday").text(data["maxbaroyest"]);
                $("#realtimeMinBaroYesterdayTime").text("(kl. " + data["minbaroyestt"] + ")");
                $("#realtimeMaxBaroYesterdayTime").text("(kl. " + data["maxbaroyestt"] + ")");
                $("#wdBaroMinMonth").text(data["mrecordlowbaro"] + " hPa");
                $("#wdMinBaroMonthTime").text("(d. " + data["mrecordlowbaroday"] + "/" + data["mrecordlowbaromonth"] + "-" + data["mrecordlowbaroyear"] + ")");
                $("#wdBaroMaxMonth").text(data["mrecordhighbaro"] + " hPa");
                $("#wdMaxBaroMonthTime").text("(d. " + data["mrecordhighbaroday"] + "/" + data["mrecordhighbaromonth"] + "-" + data["mrecordhighbaroyear"] + ")");
                $("#wdBaroMinYear").text(data["yrecordlowbaro"] + " hPa");
                $("#wdMinBaroYearTime").text("(d. " + data["yrecordlowbaroday"] + "/" + data["yrecordlowbaromonth"] + "-" + data["yrecordlowbaroyear"] + ")");
                $("#wdBaroMaxYear").text(data["yrecordhighbaro"] + " hPa");
                $("#wdMaxBaroYearTime").text("(d. " + data["yrecordhighbaroday"] + "/" + data["yrecordhighbaromonth"] + "-" + data["yrecordhighbaroyear"] + ")");
            }
        }
    });

}

function convertWindSpeed ( rawwind ) {
    return Math.round((rawwind * 0.514444444) * 100)/100;
}

function convertWindDir (winddir)
// Take wind direction value, return the
// text label based upon 16 point compass -- function by beeker425
//  see http://www.weather-watch.com/smf/index.php/topic,20097.0.html
{
    var $windlabel = new Array("N", "NNØ", "NØ", "ØNØ", "Ø", "ØSØ", "SØ", "SSØ", "S", "SSV", "SV", "VSV", "V", "VNV", "NV", "NNV");
    return $windlabel[Math.floor(((parseInt(winddir) + 11) / 22.5) % 16 )];
}

function dageEllerDag (numberString) {
    if (parseInt(numberString) == 1) {
        return "dag";
    } else {
        return "dage";
    }
}

function jsonEqual(a,b) {
    return JSON.stringify(a) === JSON.stringify(b);
}

