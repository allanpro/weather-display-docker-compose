
<head>
    <title>juelsminde-vejret.dk</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Vejret i Juelsminde LIVE"/>
    <meta name="author" content="Allan Lauridsen"/>

    <link rel="stylesheet" href="css/style.css">

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

    <!-- Custom Made JavaScript Dependencies -->
    <script src="./js_custom/graphing.js"></script>
    <script src="js_custom/parsing_data.js"></script>
    <script src="js_custom/main.js"></script>

    <!-- 3rd party dependencies -->
    <script src="./js/moment-with-langs.min.js"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-44682476-1', 'juelsminde-vejret.dk');
        ga('send', 'pageview');

    </script>

</head>
