<?php
// converted tagslist.txt to .\tagslist.php for php tags
// by gen-PHP-tagslist.pl - Version 1.00 - 07-Apr-2006
// Author: Ken True - webmaster-weather.org
// Edited: 20-Apr-2006 to trim unused tags
// Version 1.01 - 25-Jan-2008 -- added Windy-rain to icon list
// Version 1.02 - 24-Jun-2008 -- added variables to replace old trends-inc.html with trends-inc.php
// Version 1.03 - 27-Oct-2008 -- added Snow and WU almanac variables
// Version 1.04 - 03-Jun-2009 -- added moonrisedate/moonsetdate for wxastronomy.php
// Version 1.05 - 11-Jul-2009 -- added tags for printable flyer, alternative dashboard, high/low/avg plugins
//                               Thanks to Mike and Scott for their permission to add the above tags!
// Version 1.06 - 12-Jul-2009 -- added tags for V4.0 of alternative dashboard
// --------------------------------------------------------------------------
// allow viewing of generated source

if ( isset($_REQUEST['sce']) && strtolower($_REQUEST['sce']) == 'view' ) {
//--self downloader --
   $filenameReal = __FILE__;
   $download_size = filesize($filenameReal);
   header('Pragma: public');
   header('Cache-Control: private');
   header('Cache-Control: no-cache, must-revalidate');
   header("Content-type: text/plain");
   header("Accept-Ranges: bytes");
   header("Content-Length: $download_size");
   header('Connection: close');
   
   readfile($filenameReal);
   exit;
}
// General OR Non Weather Specific/SUN/MOON
// ========================================
$time =  '00:00';	// current time
$date =  '28/1/2017';	// current date
$sunrise =  '08:22';	// sun rise time (make sure you have the correct lat/lon
// 		            in view/sun moon)
$time_minute =  '00';	// Current minute
$time_hour =  '00';	// Current hour
$date_day =  '28';	// Current day
$date_month =  '01';	// Current month
$date_year =  '2017';	// Current year
$monthname =  'January';	// Current month name
$dayname =  'Saturday';	// Current day name
$sunset =  '16:43';	// sunset time
$moonrisedate =  '27/01/17';	// moon rise date
$moonrise =  '07:50';	// moon rise time
$moonsetdate =  '27/01/17';	// moon set date
$moonset =  '16:23';	// moon set time
$moonage =  'Moon age: 29 days,11 hours,38 minutes,0%';	// current age of the moon (days since new moon)
$moonphase =  '0%';	// Moon phase %
$moonphasename = 'New Moon'; // 10.36z addition
$marchequinox =  '10:29 UTC 20 marts 2017';	// March equinox date
$junesolstice =  '04:25 UTC 21 juni 2017';	// June solstice date
$sepequinox =  '20:02 UTC 22 september 2017';	// September equinox date
$decsolstice =  '16:29 UTC 21 december 2017';	// December solstice date
$moonperihel =  '16:19 UTC 3 januar 2018';	// Next Moon perihel date
$moonaphel =  '01:12 UTC 5 juli 2017';	// Next moon perihel date
$moonperigee =  '14:10 UTC 6 februar 2017';	// Next moon perigee date
$moonapogee =  '21:15 UTC 18 februar 2017';	// Next moon apogee date
$newmoon =  '06:54 UTC 29 december 2016';	// Date/time of the next/last new moon
$nextnewmoon =  '00:08 UTC 28 januar 2017';	// Date/time of the next new moon for next month
$firstquarter =  '19:48 UTC 5 januar 2017';	// Date/time of the next/last first quarter moon
$lastquarter =  '22:14 UTC 19 januar 2017';	// Date/time of the next/last last quarter moon
$fullmoon =  '11:35 UTC 12 januar 2017';	// Date/time of the next/last full moon
$fullmoondate =  '12 januar 2017';	// Date of the next/last full moon (date only)
$suneclipse =  '15:54 UTC 26 februar 2017 Eclipse Circular';	// Next sun eclipse
$mooneclipse =  '20:22 UTC 7 august 2017 Eclipse Total';	// Next moon eclipse date
$easterdate =  '16 april 2017';	// Next easter date
$chinesenewyear =  '28 januar 2017 ()';	// Chinese new year
$hoursofpossibledaylight =  '08:21';	// Total hours/minutes of possible daylight for today
//
$weatherreport =  'Dry';	// current weather conditions from selected METAR
$stationaltitude =  '0';	// Station altitude, feet, as set in the units setup
// this under setup)
$stationlatitude =  '055:42:18';	// Latitude (from the sun moon rise/set setup)
$stationlongitude =  '-009:58:50';	// Longtitude (from the sun moon rise/set setup)
$windowsuptime = '9 Days 12 Hours 16 Minutes 14 Seconds'; // uptime for windows on weather pc
$freememory = '922MB'; // amount of free memory on the pc
$Startimedate = '18:47:09 29-11-2016'; // Time/date WD was started

$NOAAEvent = 'NO CURRENT ADVISORIES'; // NOAA Watch/Warning/Advisory
$noaawarningraw = '---'; // NOAA RAW watch/warning/advisory

// ADVISORIES)
// ADVISORIES)

$wdversion = '10.37R' . '-(b' . '266' . ')';	// Weather Display version number you are running
$wdversiononly = '10.37R';
$wdbuild   = '266';       // Weather Display build number you are running
// ---....What it says - for the weather warning....
// 
// this under setup)
$noaacityname =  'Juelsminde';	// City name,from the noaa setup (in the av/ext setup)
// 
// level...enter this under setup)
// 
$timeofnextupdate =  '00:05';	// Time of next Update/Upload of the weather data to your web page (based on the web table update 
// 
// time)
// 
// must be first selected),,,,repeat up to day 8
$heatcolourword =  '---';	// How hot/cold it feels at the moment, based on the humidex, used with the conditionscolour.jpg 
// 
// 
// Temperature/Humidity
// ====================
// Current:
// --------
$temperature =  '-0.8&deg;C';	// temperature
$tempnodp = '-1'; // temperature, no decimal place
$humidity =  '92';	// humidity
$dewpt =  '-2.0&deg;C';	// dew point
$maxtemp =  '-0.8&deg;C';	// today's maximum temperature
$maxtempt =  '00:00';	// time this occurred
$mintemp =  '-0.8&deg;C';	// today's minimum temperature
$mintempt =  '00:00';	// time this occurred
// VP soil temperature)
$feelslike =  '-1';	// Shows heat index or humidex or windchill (if less than 16oC)

$heati =  '-0.8&deg;C';	// current heat index
$heatinodp =  '-1';	// current heat index,no decimal place
$windch =  '-0.8&deg;C';	// current wind-chill
$windchnodp =  '-1&deg;C';	// current wind-chill, no decimal place
$humidexfaren =  '25.8&deg;F';	// Humidex value in oF
$humidexcelsius =  '-3.4�C';	// Humidex value in oC

$apparenttemp =  '-2.5';	// Apparent temperature
$apparentsolartemp =  '-2.5';	// Apparent temperature in the sun (you need a solar sensor)
$apparenttempc =  '-2.5';	// Apparent temperature, �C
$apparentsolartempc =  '-2.5';	// Apparent temperature in the sun, �C (you need a solar sensor)
$apparenttempf =  '27.5';	// Apparent temperature, �F
$apparentsolartempf =  '27.5';	// Apparent temperature in the sun, �F (you need a solar sensor)
// 
$WUmaxtemp = '0.0';	// Todays average max temperature from the selected Wunderground almanac station
$WUmintemp = '0.0';	// Todays average min temperature from the selected Wunderground almanac station
// 
$WUmaxtempr = '0.0';	// Todays record max temperature from the selected Wunderground almanac station
$WUmintempr = '0.0';	// Todays record min temperature from the selected Wunderground almanac station
$WUmaxtempryr = '0';	// Year that it occured
$WUmintempryr = '0';	// year that it occured
// 
// 
// Yesterday:
// ----------
$tempchangehour =  '-0.1&deg;C/last hr';	// Temperature change in the last hour
$maxtempyest =  '3.0 &deg;C';	// Yesterday's max temperature
$maxtempyestt =  '14:25';	// Time of yesterday's max temperature
$mintempyest =  '-1.1 &deg;C';	// Yesterday's min temperature
$mintempyestt =  '23:15';	// Time of yesterday's min temperature
// 
// 
// Trends:
// -------
$temp24hoursago =  '0.3';	// The temperature 24 hours ago
$humchangelasthour =  '+2';	// Humidity change last hour
$dewchangelasthour =  '+0.2';	// Dew point change last hour
$barochangelasthour =  '+0.2';	// Baro change last hour
// 
// Wind
// ====
// Current:
// --------
// 
$avgspd =  '0.0 m/s';	// average wind speed (current)
$gstspd =  '0.0 m/s';	// current/gust wind speed
$maxgst =  '0.0 m/s';	// today's maximum wind speed
$maxgstt =  '08:55';	// time this occurred
$maxgsthr =  '0 m/s';	// maximum gust last hour
$dirdeg =  '188 &deg;';	// wind direction (degrees)
$dirlabel =  'S';	// wind direction (NNE etc)
//$maxgustlastimediatehourtime =  '';	//   time that the max gust last prior 1 hour occured
$avwindlastimediate10 =  '0.0 m/s';	// Average wind for the last immediate 10 minute period
// $avdir10minute =  '188&deg;';	// average ten minute wind direction (degrees)

$beaufortnum ='0'; //Beaufort wind force number
$currbftspeed = '0 bft'; //Current Beaufort wind speed

$bftspeedtext = 'Calm'; //Beaufort scale in text (i.e Fresh Breeze)
// 
// 
// Baromometer
// ===========
// Current:
// --------
$baro = '1020.2 hpa';  // current barometer
$baroinusa2dp =  '30.13 inches';	// Current barometer reading in inches, 2 decimal places only.
$trend =  '+0.2hPa/hr';	// amount of change in the last hour
$pressuretrendname =  'Steady';	// pressure trend (i.e. "falling"), last hour
$pressuretrendname3hour =  'Steady';	// pressure trend (i.e. "falling"), last 3 hours

$vpforecasttext = 'increasing clouds with little temp. change. precipitation possible within 24 to 48 hrs.';	// Forecast text from the Davis VP
// 
// 
// Rain
// ====
// Current:
// --------
$dayrn =  '  0.0 mm';	// today's rain
$monthrn =  '21.2 mm';	// rain so far this month
$yearrn =  '21.2 mm';	// rain so far this year
$dayswithnorain =  '3';	// Consecutative days with no rain
$dayswithrain =  '13';	// Days with rain for the month
$dayswithrainyear =  '13';	// Days with rain for the year
$currentrainratehr =  '0.0';	// Current rain rate, mm/hr (or in./hr)
$maxrainrate =  '0.0';	// Max rain rate,for the day, mm/min (or in./min)
$maxrainratehr =  '0.0';	// Max rain rate,for the day, mm/hr (or in.mm)
$maxrainratetime =  '00:00';	// Time that occurred
// Yesterday:
// ----------
$yesterdayrain =  '0.0 mm';	// Yesterday rain
//
$vpstormrainstart = '0/0/0';  //Davis VP Storm rain start date
$vpstormrain = '0.0 mm';           //Davis VP Storm rain value
//
// 
// Sunshine/Solar/ET
// =================
$VPsolar =  '0';	//  Solar energy number (W/M2)
$VPuv =  '0.0';	// UV number 
$highsolar =  '0';	// Daily high solar (for Davis VP and Grow stations)
$highuv =  '0.0';	// Daily high UV (for Davis VP stations)
$currentsolarpercent =  '0 %';	// Current solar percent for stations with a temperature solar sensor (like the dallas 1 wire)
$highsolartime =  '00:00';	// Time that the daily high solar occured
$lowsolartime =  '00:00';	// Time that the daily low solar occured
$highuvtime =  '00:00';	// Time that the daily high UV occured
$lowuvtime =  '00:00';	// Time that the daily low UV occured
$highuvyest =  '0.0';	// Yesterday's high UV
$highuvyesttime =  '00:00';	// Time of yesterday's high UV
$burntime =  '---';	// Time (minutes) to burn (normal skin) at the current UV rate, from the Davis VP with UV sensor
// 
// the solar setup.
// 
// 
// Number of resynchronizations, The largest number of packets in a row that were received., and the number of CRC errors 
// 
// detected. 
// 
// 
// Record Readings
// ===============
// 
// for current month to date:
// 
$mrecordwindgust =  '19.6';	// All time record high wind gust
$mrecordhighgustday =  '11';	// Day of record high wind gust
// 
// 
// Snow
// =====
// 
$snowseasonin = '0';	// Snow for season you have entered under input daily weather, inches
$snowmonthin = '0';	// Snow for month you have entered under input daily weather, inches
$snowtodayin = '0.00';	// Snow for today you have entered under input daily weather, inches
$snowseasoncm = '0';	// Snow for season you have entered under input daily weather, cm
$snowmonthcm = '0';	// Snow for month you have entered under input daily weather, cm
$snowtodaycm = '0.0';	// Snow for today you have entered under input daily weather, cm
$snowyesterday = '0';	// Yesterdays' snow
$snowheight = '402';	// Estimated height snow will fall at
$snowheightnew = '0';	// Estimated height snow will fall at, new formula
// 
$snownowin = '0.00';	// Current snow depth, inches.
$snownowcm = '0.0';	// Current snow depth, cm.
// 
$snowrain = '0.0';	// Rain measure by a heated rain gauge when temp below freezing times 10 to give estimated snow fall
$snowdaysthismonth = '0';	// Days with snow this month
$snowdaysthisyear = '0';	// Days with snow this year
//
// tags needed for trends-inc.php
//
$temp0minuteago = '-0.8';  // ****this one is needed for all the others to work
$wind0minuteago = '0.0';
$gust0minuteago = '0.0';
$dir0minuteago = ' S ';
$hum0minuteago = '92';
$dew0minuteago = '-2.0';
$baro0minuteago = '1020.1';
$rain0minuteago = '0.0';
$VPsolar0minuteago = '0';
$VPuv0minuteago = '0.0';

$temp5minuteago = '-0.8';  
$wind5minuteago = '0.0';
$gust5minuteago = '0.0';
$dir5minuteago = ' S ';
$hum5minuteago = '92';
$dew5minuteago = '-2.0';
$baro5minuteago = '1020.1';
$rain5minuteago = '0.0';
$VPsolar5minuteago = '0';
$VPuv5minuteago = '0.0';

$temp10minuteago = '-0.9';  
$wind10minuteago = '0.0';
$gust10minuteago = '0.0';
$dir10minuteago = ' S ';
$hum10minuteago = '92';
$dew10minuteago = '-2.1';
$baro10minuteago = '1020.2';
$rain10minuteago = '0.0';
$VPsolar10minuteago = '0';
$VPuv10minuteago = '0.0';

$temp15minuteago = '-1.0';  
$wind15minuteago = '0.0';
$gust15minuteago = '0.0';
$dir15minuteago = ' S ';
$hum15minuteago = '91';
$dew15minuteago = '-2.3';
$baro15minuteago = '1020.1';
$rain15minuteago = '0.0';
$VPsolar15minuteago = '0';
$VPuv15minuteago = '0.0';

$temp20minuteago = '-1.0';  
$wind20minuteago = '0.0';
$gust20minuteago = '0.0';
$dir20minuteago = ' S ';
$hum20minuteago = '91';
$dew20minuteago = '-2.3';
$baro20minuteago = '1020.1';
$rain20minuteago = '0.0';
$VPsolar20minuteago = '0';
$VPuv20minuteago = '0.0';

$temp30minuteago = '-1.0';  
$wind30minuteago = '0.0';
$gust30minuteago = '0.0';
$dir30minuteago = ' S ';
$hum30minuteago = '91';
$dew30minuteago = '-2.3';
$baro30minuteago = '1020.0';
$rain30minuteago = '0.0';
$VPsolar30minuteago = '0';
$VPuv30minuteago = '0.0';

$temp45minuteago = '-1.0';  
$wind45minuteago = '0.0';
$gust45minuteago = '0.0';
$dir45minuteago = ' S ';
$hum45minuteago = '90';
$dew45minuteago = '-2.4';
$baro45minuteago = '1020.0';
$rain45minuteago = '0.0';
$VPsolar45minuteago = '0';
$VPuv45minuteago = '0.0';

$temp60minuteago = '-0.8';  
$wind60minuteago = '0.0';
$gust60minuteago = '0.0';
$dir60minuteago = ' S ';
$hum60minuteago = '90';
$dew60minuteago = '-2.2';
$baro60minuteago = '1020.0';
$rain60minuteago = '0.0';
$VPsolar60minuteago = '0';
$VPuv60minuteago = '0.0';

$temp75minuteago = '-0.2';  
$wind75minuteago = '0.0';
$gust75minuteago = '0.0';
$dir75minuteago = ' S ';
$hum75minuteago = '90';
$dew75minuteago = '-1.7';
$baro75minuteago = '1019.9';
$rain75minuteago = '0.0';
$VPsolar75minuteago = '0';
$VPuv75minuteago = '0.0';

$temp90minuteago = '-0.1';  
$wind90minuteago = '0.5';
$gust90minuteago = '1.0';
$dir90minuteago = ' S ';
$hum90minuteago = '90';
$dew90minuteago = '-1.5';
$baro90minuteago = '1019.9';
$rain90minuteago = '0.0';
$VPsolar90minuteago = '0';
$VPuv90minuteago = '0.0';

$temp105minuteago = '-0.1';  
$wind105minuteago = '0.5';
$gust105minuteago = '1.5';
$dir105minuteago = ' S ';
$hum105minuteago = '90';
$dew105minuteago = '-1.5';
$baro105minuteago = '1020.0';
$rain105minuteago = '0.0';
$VPsolar105minuteago = '0';
$VPuv105minuteago = '0.0';

$temp120minuteago = '-0.0';  
$wind120minuteago = '2.1';
$gust120minuteago = '2.6';
$dir120minuteago = 'SSW';
$hum120minuteago = '91';
$dew120minuteago = '-1.3';
$baro120minuteago = '1020.0';
$rain120minuteago = '0.0';
$VPsolar120minuteago = '0';
$VPuv120minuteago = '0.0';

$VPet = '0.0';
$VPetmonth = '0.0';
$dateoflastrainalways = '25/1/2017';
$highbaro = '1020.2 hPa';
$highbarot = '00:00';
$highsolaryest = '0.0';
$highsolaryesttime = '00:00';
$hourrn = '0.0';
$maxaverageyest = '4.4 m/s   E';
$maxaverageyestt = '08:55';
$maxavgdirectionletter = '  E';
$maxavgspd = '0.0 m/s';
$maxavgspdt = '08:55';
$maxbaroyest = '1026.2 hPa';
$maxbaroyestt = '00:00';
$maxgstdirectionletter = '  E';
$maxgustyest = '6.7 m/s   E';
$maxgustyestt = '08:55';
$mcoldestdayonrecord = '-4.2&deg;C  on: 05 Jan 2017';
$mcoldestnightonrecord = '-5.9&deg;C  on: 06 Jan 2017';
$minchillyest = '-4.2 &deg;C';
$minchillyestt = '08:55';
$minwindch = '-0.8 &deg;C';
$minwindcht = '00:00';
$mrecordhighavwindday = '11';
$mrecordhighavwindmonth = '1';
$mrecordhighavwindyear = '2017';
$mrecordhighbaro = '1040.7';
$mrecordhighbaroday = '6';
$mrecordhighbaromonth = '1';
$mrecordhighbaroyear = '2017';
$mrecordhighgustmonth = '1';
$mrecordhighgustyear = '2017';
$mrecordhightemp = '8.4';
$mrecordhightempday = '1';
$mrecordhightempmonth = '1';
$mrecordhightempyear = '2017';
$mrecordlowchill = '-10.7';
$mrecordlowchillday = '5';
$mrecordlowchillmonth = '1';
$mrecordlowchillyear = '2017';
$mrecordlowtemp = '-6.5';
$mrecordlowtempday = '5';
$mrecordlowtempmonth = '1';
$mrecordlowtempyear = '2017';
$mrecordwindspeed = '14.7';
$mwarmestdayonrecord = '6.2&deg;C  on: 21 Jan 2017';
$mwarmestnightonrecord = '8.0&deg;C  on: 01 Jan 2017';
$raincurrentweek = '0.4';
$raintodatemonthago = '0.0';
$raintodateyearago = '0.0';
$timeoflastrainalways = ' 17:37';
$windruntodatethismonth = '6015.03 km';
$windruntodatethisyear = '6015.03 km';
$windruntoday = '0.00';
$yesterdaydaviset = '0.0';
$yrecordhighavwindday = '11';
$yrecordhighavwindmonth = '1';
$yrecordhighavwindyear = '2017';
$yrecordhighbaro = '1040.7';
$yrecordhighbaroday = '6';
$yrecordhighbaromonth = '1';
$yrecordhighbaroyear = '2017';
$yrecordhighgustday = '11';
$yrecordhighgustmonth = '1';
$yrecordhighgustyear = '2017';
$yrecordhightemp = '8.4';
$yrecordhightempday = '1';
$yrecordhightempmonth = '1';
$yrecordhightempyear = '2017';
$yrecordlowchill = '-10.7';
$yrecordlowchillday = '5';
$yrecordlowchillmonth = '1';
$yrecordlowchillyear = '2017';
$yrecordlowtemp = '-6.5';
$yrecordlowtempday = '5';
$yrecordlowtempmonth = '1';
$yrecordlowtempyear = '2017';
$yrecordwindgust = '19.6';
$yrecordwindspeed = '14.7';
$daysTmaxGT30C = '0';
$daysTmaxGT25C = '0';
$daysTminLT0C = '15';
$daysTminLTm15C = '0';

// end of trends-inc.php variables

//  
   // CURRENT CONDITIONS ICONS FOR clientraw.txt
   // create array for icons. There are 35 possible values in clientraw.txt
   // It would be simpler to do this with array() but to make it easier to 
   // modify each element is defined individually. Each index [#] corresponds
   // to the value provided in clientraw.txt
   $icon_array[0] =  'day_clear.gif';            // imagesunny.visible
   $icon_array[1] =  'night_clear.gif';          // imageclearnight.visible
   $icon_array[2] =  'day_partly_cloudy.gif';    // imagecloudy.visible
   $icon_array[3] =  'day_partly_cloudy.gif';    // imagecloudy2.visible
   $icon_array[4] =  'night_partly_cloudy.gif';  // imagecloudynight.visible
   $icon_array[5] =  'day_partly_cloudy.gif';            // imagedry.visible
   $icon_array[6] =  'fog.gif';                  // imagefog.visible
   $icon_array[7] =  'haze.gif';                 // imagehaze.visible
   $icon_array[8] =  'day_heavy_rain.gif';       // imageheavyrain.visible
   $icon_array[9] =  'day_mostly_sunny.gif';     // imagemainlyfine.visible
   $icon_array[10] =  'mist.gif';                // imagemist.visible
   $icon_array[11] =  'fog.gif';                 // imagenightfog.visible
   $icon_array[12] =  'night_heavy_rain.gif';    // imagenightheavyrain.visible
   $icon_array[13] =  'night_cloudy.gif';        // imagenightovercast.visible
   $icon_array[14] =  'night_rain.gif';          // imagenightrain.visible
   $icon_array[15] =  'night_light_rain.gif';    // imagenightshowers.visible
   $icon_array[16] =  'night_snow.gif';          // imagenightsnow.visible
   $icon_array[17] =  'night_tstorm.gif';        // imagenightthunder.visible
   $icon_array[18] =  'day_cloudy.gif';          // imageovercast.visible
   $icon_array[19] =  'day_partly_cloudy.gif';   // imagepartlycloudy.visible
   $icon_array[20] =  'day_rain.gif';            // imagerain.visible
   $icon_array[21] =  'day_rain.gif';            // imagerain2.visible
   $icon_array[22] =  'day_light_rain.gif';      // imageshowers2.visible
   $icon_array[23] =  'sleet.gif';               // imagesleet.visible
   $icon_array[24] =  'sleet.gif';               // imagesleetshowers.visible
   $icon_array[25] =  'snow.gif';                // imagesnow.visible
   $icon_array[26] =  'snow.gif';                // imagesnowmelt.visible
   $icon_array[27] =  'snow.gif';                // imagesnowshowers2.visible
   $icon_array[28] =  'day_clear.gif.gif';       // imagesunny.visible
   $icon_array[29] =  'day_tstorm.gif';          // imagethundershowers.visible
   $icon_array[30] =  'day_tstorm.gif';          // imagethundershowers2.visible
   $icon_array[31] =  'day_tstorm.gif';          // imagethunderstorms.visible
   $icon_array[32] =  'tornado.gif';             // imagetornado.visible
   $icon_array[33] =  'windy.gif';               // imagewindy.visible
   $icon_array[34] =  'day_partly_cloudy.gif';   // stopped raining
   $icon_array[35] =  'windyrain.gif';           // Wind+rain
   $iconnumber = '1';                // icon number

   $current_icon = $icon_array[1]; // name of our condition icon
// ----------------------------------------------------------------------------------
//   $current_summary = 'Dry' . "<br />" . '--- ';
   $weathercond = 'Dry';
   $Currentsolardescription = '--- ';
   $current_summary = $Currentsolardescription;
   $current_summary = preg_replace('|^/[^/]+/|','',$current_summary);
   $current_summary = preg_replace('|\\\\|',", ",$current_summary);
   $current_summary = preg_replace('|/|',", ",$current_summary);
//  
//  
$cloudheightfeet =  '468';	// Estimated cloud base height, feet, (based on dew point, and you height above sea  level...enter
$cloudheightmeters =  '143';	// Estimated cloud base height, metres, (based on dew point, and you height above sea

// end of stock testtags.txt

// ----------------------------------------------------------------------------------------------------

// begin mchallis tags added to testtags.txt for printable flyer
$maxgsthrtime = '';        // time that the max gust last prior 1 hour occured


$minbaroyest  = '1018.9 hPa';
$minbaroyestt = '16:24';




$mrecordlowbaro = '984.0';
$mrecordlowbaroday = '11';
$mrecordlowbaromonth = '1';
$mrecordlowbaroyear = '2017';




$yrecordlowbaro = '984.0';
$yrecordlowbaroday = '11';
$yrecordlowbaromonth = '1';
$yrecordlowbaroyear = '2017';

// end mchallis tags added to testtags.txt for printable flyer

// ----------------------------------------------------------------------------------------------------

// begin Webster Weather Live alternative dashboard plugin tags
// Note: duplicated named tags commented out
// Modifications for Webster Weather LIVE's Dashboard Modifications
// NOTE: See instruction and comments for directions
// Add this code in right before the section called
// General OR Non Weather Specific/SUN/MOON in the file
// in your c:\wDisplay\webfiles directory
// ========================================================
$vpissstatus = 'Ok';      // VP ISS Status
$vpreception2 = '100%'; // VP Current reception %  *** NEW IN V1.01
$vpconsolebattery = '4.7'; // VP Console Battery Volts *** NEW IN V1.01
$firewi = '0.0'; // Fire Weather Index
$avtempweek = '1.9';    // Average Weekly Temp
$warmestdayonrecord = '23.7&deg;C  on: 25 Aug 2016';  //Warmest Day on Record
$coldestdayonrecord = '-4.9&deg;C  on: 26 Jan 2014';  //coldest Day on Record
$warmestnightonrecord = '22.1&deg;C  on: 26 Jul 2014';  //Warmest Night on Record
$coldestnightonrecord = '-5.9&deg;C  on: 06 Jan 2017';  //coldest Day on Record
$recordhightemp = '28.7';   // Record high temp
$recordhightempmonth = '8';   // Record high temp month
$recordhightempday = '26';   // Record high temp day
$recordhightempyear = '2016';   // Record high temp year
$recordlowtemp = '-7.8';   // Record low temp
$recordlowtempmonth = '1';   // Record low temp month
$recordlowtempday = '21';   // Record low temp day
$recordlowtempyear = '2016';   // Record low temp year
$recordlowchillmonth = '1';   // Record low temp month
$recordlowchillday = '26';   // Record low temp day
$recordlowchillyear = '2014';   // Record low temp year
$hddday = '17.6';        // Heating Degree for day
$hddmonth = '446.5';    // Heating Degree for month to date
$hddyear = '446.5';    // Heating Degree for year to date
$cddday = '0.0';        // Cooling Degree for day
$cddmonth = '0.0';    // Cooling Degree for month to date
$cddyear = '0.0';    // Cooling Degree for year to date
$minchillweek = '-5.5';  // Minimum Wind Chill over past 7 days 
$maxheatweek = '7.9';  // Maximum Heat Index for the Week *** NEW IN V2.00
$recordlowchill = '-16.6';  //record low windchill
$airdensity = '1.302';  //air density
$solarnoon = '12:32'; // Solar noon
$changeinday = '00:04:06';  // change in day length since yesterday
// You can comment out the next 6 WU tags if you don't use the Weather Underground Records option
// Snow tags for USA - Comment out if using CM
// Snow tags for Metric - unComment out if using CM
// End of snow tages to change
$etcurrentweek = '0.0'; // ET total for the last 7 days
// 
// NEW TAGS in Version 2.75
//
$sunshinehourstodateday = '00:00';
$sunshinehourstodatemonth = '00:00';
$maxsolarfortime = '0';
$wetbulb = '-1.2&deg;C';
//
// NEW TAGS in Version 2.80
// Not needed if not using Lightning display!
//
$lighteningcountlasthour = '0';
$lighteningcountlastminute = '0';
$lighteningcountlast5minutes = '0';
$lighteningcountlast12hour = '0';
$lighteningcountlast30minutes = '0';
$lighteningcountlasttime = '';
$lighteningcountmonth = '0';
$lighteningcountyear = '0';
//  End of Lightning tags
//
//  NEW TAGS IN VERSION 3.00
//
$chandler = '-2.3';

//  New tags in Version 4.00
$maxdew = '-2.0 �C';
$maxdewt = '00:00';
$mindew = '-2.0 �C';
$mindewt = '00:00';
$maxdewyest = '0.3 �C';
$maxdewyestt = '13:30';
$mindewyest = '-2.5 �C';
$mindewyestt = '23:15';
$mrecordhighdew = '6.9';
$mrecordhighdewmonth = '1';
$mrecordhighdewday = '1';
$mrecordlowdew = '-9.8';
$mrecordlowdewmonth = '1';
$mrecordlowdewday = '5';
$yrecordhighdew = '6.9';
$yrecordhighdewmonth = '1';
$yrecordhighdewday = '1';
$yrecordlowdew = '-9.8';
$yrecordlowdewmonth = '1';
$yrecordlowdewday = '5';
$recordhighdew = '21.7';
$recordhighdewyear = '2015';
$recordhighdewmonth = '7';
$recordhighdewday = '5';
$recordlowdew = '-9.8';
$recordlowdewyear = '2014';
$recordlowdewmonth = '1';
$recordlowdewday = '25';
$stationname = 'juelsminde-vejret.dk';
$raindifffromav = '---';
$raindifffromavyear = '21.2';
$gddmonth = '0.0';
$gddyear = '0.0';

// end of WebsterWeatherLive Alternative Dashboard plugin tags

// ----------------------------------------------------------------------------------------------------

// begin WebsterWeatherLive High/Low/Average plugin page tags
// Note: duplicated tags have been removed
//
// Modifications for Webster Weather Live's Modifications
// NOTE: These are all using US measurements!!
// Add this code in right before the section called
// General OR Non Weather Specific/SUN/MOON in the file
// in your c:\wDisplay\webfiles directory
// Version 1.1 - 17-FEB-2009 Initial Release
// Version 1.2 - 10-MAR-2009 Fixed snow tags for CM measurements
// ========================================================
$recordhighheatindex = '29.9';
$recordhighheatindexmonth = '8';   // Record high heatindex month
$recordhighheatindexday = '26';   // Record high heatindex day
$recordhighheatindexyear = '2016';   // Record high heatindex year 
$recordhighbaromonth = '10';   // Record high baro month
$recordhighbaroday = '4';   // Record high baro day
$recordhighbaroyear = '2016';   // Record high baro year 
$recordlowbaromonth = '10';   // Record low baro month
$recordlowbaroday = '28';   // Record low baro day
$recordlowbaroyear = '2013';   // Record low baro year 
$recorddailyrainmonth = '12';   // Record daily rain month
$recorddailyrainday = '26';   // Record daily rain day
$recorddailyrainyear = '2015';   // Record daily rain year
$recorddailyrain = '26.6'; // Record Daily Rain 
$maxheat = '-0.8 &deg;C';
$maxheatt = '00:00'; 
$maxheatyest = '3.0 &deg;C';  
$mrecordhighheatindex = '8.4';
$yrecordhighheatindex = '8.4';
// You can comment out the next 6 lines if you do not use Weather Underground history
// Next 4 Lines are US snow measurements (Comment out if using CM)
// Next 4 lines are METRIC snow measurements (Uncomment to use these)
// End of snow tags to change
$yeartodateavtemp = '1.8'; 
$monthtodateavtemp = '1.8'; 
$maxchillyest = '3.0 �C'; 
$monthtodatemaxgust = '19.0'; 
$recordwindspeed = '19.5'; // All Time Record Avg Wind Speed
$recordwindgust = '28.8'; // All Time Record wind gust
$monthtodateavspeed = '2.6'; // MTD average wind speed
$monthtodateavgust = '3.9'; //MTD average wind gust
$yeartodateavwind = '2.6'; // YTD average wind speed
$yeartodategstwind = '3.9'; // YTD avg wind gust
$lowbaro = '1020.1 hPa';
$lowbarot = '00:00';
$monthtodatemaxbaro = '1040.7'; // MTD average wind speed
$monthtodateminbaro = '984.0'; //MTD average wind gust
$recordhighbaro = '1041.1'; // All Time Record Avg Wind Speed
$recordlowbaro = '969.0'; // All Time Record wind gust
$recordhighavwindmonth = '12'; 
$recordhighavwindday = '5';
$recordhighavwindyear = '2013';
$recordhighgustmonth = '12';
$recordhighgustday = '5';
$recordhighgustyear = '2013';
$sunshinehourstodateyear = '00:00'; 
$sunshineyesterday = '00:00';
$mrecordhighsolar = '0.0';  
$yrecordhighsolar = '0.0';
$recordhighsolar = '0.0'; 
$recordhighsolarmonth = '8'; 
$recordhighsolarday = '22';
$recordhighsolaryear = '2013';
$mrecordhighuv = '0.0';  
$yrecordhighuv = '0.0';
$recordhighuv = '0.0'; 
$recordhighuvmonth = '8'; 
$recordhighuvday = '22';
$recordhighuvyear = '2013';
$avtempsincemidnight = '-0.8';
$yesterdayavtemp = '-0.2';
$avgspeedsincereset = '1.7';
$maxheatyestt = '14:25';
$recorddayswithrain = '22';
$recorddayswithrainmonth = '12';
$recorddayswithrainday = '25';
$recorddayswithrainyear = '2014';
$mrecorddailyrain = '11.2';
$yrecorddailyrain = '11.2';
$mrecordhighheatindexday = '1';
$mrecordhighheatindexmonth = '1';
$yrecordhighheatindexday = '1';
$yrecordhighheatindexmonth = '1';
$mrecordhighsolarday = '22';
$mrecordhighsolarmonth = '8';
$yrecordhighsolarday = '22';
$yrecordhighsolarmonth = '8';
$mrecordhighuvday = '22';
$mrecordhighuvmonth = '8';
$yrecordhighuvday = '22';
$yrecordhighuvmonth = '8';
$windrunyesterday = '145.96';
$recordhighwindrun = '730.0';
$recordhighwindrunday = '5';
$recordhighwindrunmth = '12';
$recordhighwindrunyr = '2015';
$currentwdet = '0.0';
$yesterdaywdet = '0.0';
$highhum = '92';
$highhumt = '00:00';
$lowhum = '92';
$lowhumt = '00:00';
$maxhumyest = '94';
$maxhumyestt = '00:00';
$minhumyest = '79';
$minhumyestt = '14:42';
$mrecordhighhum = '98';
$mrecordhighhummonth = '1';
$mrecordhighhumday = '19';
$mrecordlowhum = '64';
$mrecordlowhummonth = '1';
$mrecordlowhumday = '4';
$yrecordhighhum = '98';
$yrecordhighhummonth = '1';
$yrecordhighhumday = '19';
$yrecordlowhum = '64';
$yrecordlowhummonth = '1';
$yrecordlowhumday = '4';
$recordhighhum = '99';
$recordhighhumyear = '2015';
$recordhighhummonth = '11';
$recordhighhumday = '7';
$recordlowhum = '24';
$recordlowhumyear = '2016';
$recordlowhummonth = '5';
$recordlowhumday = '13';

// 
// Monthly High/low/avg Hometownzone Mod
// You can ship this section if you do not plan
// to use the Monthly.php script 
//
$recordhightempjan = '10.6';
$recordlowtempjan = '-7.8';
$avtempjan = '2.0';
$avrainjan = '53.6';
$recordhightempfeb = '9.6';
$recordlowtempfeb = '-4.4';
$avtempfeb = '2.9';
$avrainfeb = '28.0';
$recordhightempmar = '15.2';
$recordlowtempmar = '-1.3';
$avtempmar = '5.0';
$avrainmar = '29.5';
$recordhightempapr = '18.9';
$recordlowtempapr = '-0.8';
$avtempapr = '7.5';
$avrainapr = '31.3';
$recordhightempmay = '24.3';
$recordlowtempmay = '1.7';
$avtempmay = '11.6';
$avrainmay = '38.5';
$recordhightempjun = '25.7';
$recordlowtempjun = '5.6';
$avtempjun = '14.8';
$avrainjun = '34.4';
$recordhightempjul = '26.7';
$recordlowtempjul = '7.4';
$avtempjul = '17.2';
$avrainjul = '35.0';
$recordhightempaug = '28.7';
$recordlowtempaug = '7.2';
$avtempaug = '16.7';
$avrainaug = '36.2';
$recordhightempsep = '26.1';
$recordlowtempsep = '6.3';
$avtempsep = '14.5';
$avrainsep = '49.2';
$recordhightempoct = '18.9';
$recordlowtempoct = '3.8';
$avtempoct = '11.0';
$avrainoct = '60.7';
$recordhightempnov = '15.4';
$recordlowtempnov = '-5.1';
$avtempnov = '6.7';
$avrainnov = '57.8';
$recordhightempdec = '12.2';
$recordlowtempdec = '-4.6';
$avtempdec = '5.3';
$avraindec = '64.0';

// end of Webster Weather Live tags for High/Low/Average plugin
// ----------------------------------------------------------------------------------------------------
// end of testtags.txt/testtags.php
?>
