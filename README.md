# Weather Display Docker Compose #

This is a Docker Compose project for the Weather Display MySQL database. It comes with web-server (PHP), MySQL server, phpmyadmin service for the MySQL service and a SFTP-service for uploading Weather Display files.

### What is this? ###

* Docker Compose files to get up and running really easy with 4 inter-connected containers
    * Database (preloaded with schema and indexes for performance)
    * PHP Application (with clean web-interface)
    * Database Administration (phpMyAdmin)
    * SFTP Server (for uploading realtime files from WD)
* 2 custommized Docker Images
    * wd-mysql which is a MySQL database with a bootstrapped Weather Display database
    * wd-php which is a PHP application that presents data from the database and WD ftp-files in a cool design. See example here: http://www.juelsminde-vejret.dk 
    
### How does it work? ###

- Check out repo.
- Download and install Docker.
- Copy docker-compose-dev-example.yml to docker-compose-dev.yml and substitute parameters.
- Run Docker Compose: 
```
#!bash

docker-compose -f docker-compose-dev.yml up -d
```


### Contact ###

All Pull Requests are very welcome.